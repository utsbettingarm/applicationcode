classdef teachApp < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                    matlab.ui.Figure
        GridLayout                  matlab.ui.container.GridLayout
        LeftPanel                   matlab.ui.container.Panel
        UIAxes                      matlab.ui.control.UIAxes
        RightPanel                  matlab.ui.container.Panel
        Button                      matlab.ui.control.Button
        Button_2                    matlab.ui.control.Button
        SetButton                   matlab.ui.control.Button
        RadsLabel                   matlab.ui.control.Label
        LinktomaniulateButtonGroup  matlab.ui.container.ButtonGroup
        Joint1Button                matlab.ui.control.RadioButton
        Joint2Button                matlab.ui.control.RadioButton
        Joint3Button                matlab.ui.control.RadioButton
        Joint4Button                matlab.ui.control.RadioButton
        MenuButton                  matlab.ui.control.Button
        EditField                   matlab.ui.control.NumericEditField
        ESTOPButton                 matlab.ui.control.Button
    end

    % Properties that correspond to apps with auto-reflow
    properties (Access = private)
        onePanelWidth = 576;
    end

    % Callbacks that handle component events
    methods (Access = private)

        % Button pushed function: MenuButton
        function MenuButtonPushed(app, event)
            MainMenuApp
            closereq
        end

        % Changes arrangement of the app based on UIFigure width
        function updateAppLayout(app, event)
            currentFigureWidth = app.UIFigure.Position(3);
            if(currentFigureWidth <= app.onePanelWidth)
                % Change to a 2x1 grid
                app.GridLayout.RowHeight = {480, 480};
                app.GridLayout.ColumnWidth = {'1x'};
                app.RightPanel.Layout.Row = 2;
                app.RightPanel.Layout.Column = 1;
            else
                % Change to a 1x2 grid
                app.GridLayout.RowHeight = {'1x'};
                app.GridLayout.ColumnWidth = {465, '1x'};
                app.RightPanel.Layout.Row = 1;
                app.RightPanel.Layout.Column = 2;
            end
        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.AutoResizeChildren = 'off';
            app.UIFigure.Position = [100 100 640 480];
            app.UIFigure.Name = 'UI Figure';
            app.UIFigure.SizeChangedFcn = createCallbackFcn(app, @updateAppLayout, true);

            % Create GridLayout
            app.GridLayout = uigridlayout(app.UIFigure);
            app.GridLayout.ColumnWidth = {465, '1x'};
            app.GridLayout.RowHeight = {'1x'};
            app.GridLayout.ColumnSpacing = 0;
            app.GridLayout.RowSpacing = 0;
            app.GridLayout.Padding = [0 0 0 0];
            app.GridLayout.Scrollable = 'on';

            % Create LeftPanel
            app.LeftPanel = uipanel(app.GridLayout);
            app.LeftPanel.Layout.Row = 1;
            app.LeftPanel.Layout.Column = 1;

            % Create UIAxes
            app.UIAxes = uiaxes(app.LeftPanel);
            title(app.UIAxes, 'Teach/Jog/')
            xlabel(app.UIAxes, 'X')
            ylabel(app.UIAxes, 'Y')
            app.UIAxes.Position = [8 8 450 465];

            % Create RightPanel
            app.RightPanel = uipanel(app.GridLayout);
            app.RightPanel.Layout.Row = 1;
            app.RightPanel.Layout.Column = 2;

            % Create Button
            app.Button = uibutton(app.RightPanel, 'push');
            app.Button.Position = [38 398 100 22];
            app.Button.Text = '<';

            % Create Button_2
            app.Button_2 = uibutton(app.RightPanel, 'push');
            app.Button_2.Position = [38 367 100 22];
            app.Button_2.Text = '>';

            % Create SetButton
            app.SetButton = uibutton(app.RightPanel, 'push');
            app.SetButton.Position = [63 328 49 22];
            app.SetButton.Text = 'Set';

            % Create RadsLabel
            app.RadsLabel = uilabel(app.RightPanel);
            app.RadsLabel.Position = [38 289 33 22];
            app.RadsLabel.Text = 'Rads';

            % Create EditField
            app.EditField = uieditfield(app.RightPanel, 'numeric');
            app.EditField.Position = [38 268 100 22];

            % Create LinktomaniulateButtonGroup
            app.LinktomaniulateButtonGroup = uibuttongroup(app.RightPanel);
            app.LinktomaniulateButtonGroup.Title = 'Link to maniulate...';
            app.LinktomaniulateButtonGroup.Position = [26 121 123 118];

            % Create Joint1Button
            app.Joint1Button = uiradiobutton(app.LinktomaniulateButtonGroup);
            app.Joint1Button.Text = 'Joint 1';
            app.Joint1Button.Position = [11 72 58 22];
            app.Joint1Button.Value = true;

            % Create Joint2Button
            app.Joint2Button = uiradiobutton(app.LinktomaniulateButtonGroup);
            app.Joint2Button.Text = 'Joint 2';
            app.Joint2Button.Position = [11 50 65 22];

            % Create Joint3Button
            app.Joint3Button = uiradiobutton(app.LinktomaniulateButtonGroup);
            app.Joint3Button.Text = 'Joint 3';
            app.Joint3Button.Position = [11 28 65 22];

            % Create Joint4Button
            app.Joint4Button = uiradiobutton(app.LinktomaniulateButtonGroup);
            app.Joint4Button.Text = 'Joint 4';
            app.Joint4Button.Position = [11 7 65 22];

            % Create MenuButton
            app.MenuButton = uibutton(app.RightPanel, 'push');
            app.MenuButton.ButtonPushedFcn = createCallbackFcn(app, @MenuButtonPushed, true);
            app.MenuButton.Position = [38 34 100 22];
            app.MenuButton.Text = 'Menu';

            % Create ESTOPButton
            app.ESTOPButton = uibutton(app.RightPanel, 'push');
            app.ESTOPButton.BackgroundColor = [0.8784 0.6784 0.6784];
            app.ESTOPButton.FontWeight = 'bold';
            app.ESTOPButton.FontColor = [1 0 0];
            app.ESTOPButton.Position = [38 73 100 32];
            app.ESTOPButton.Text = 'E-STOP';

            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = teachApp

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end